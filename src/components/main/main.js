import PropTypes from 'prop-types'
import styles from './main.module.css';

export default function Main({ dataObject }) {
    const dataQuantity = 10;
    return (
        <main className={styles.main}>
            {dataObject.slice(0, dataQuantity).map((data) => (
                <div key={data.id} className={styles.item}>
                    <p className={styles.title}>{data.title}</p>
                    <img src={data.url} height={100} alt="img"/>
                </div>
            ))}
        </main>
    )
}

Main.propTypes = {
    dataObject: PropTypes.array,
};


