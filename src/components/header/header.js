import styles from './header.module.css';
import PropTypes from 'prop-types';

export default function Header({ title }) {
    return (
        <header className={styles.header}>
            <h1 className={styles.title}>{title}</h1>
        </header>
    )
}

Header.propTypes = {
    name: PropTypes.string
};
