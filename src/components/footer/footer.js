import styles from './footer.module.css';
import PropTypes from 'prop-types';

export default function Footer({ title = 'Footer title', year }) {
    return (
        <footer className={styles.footer}>
            <span>{title}</span>
            <span className={styles.copiright}>copyright&copy; {year}</span>
        </footer>
    )
}

Footer.propTypes = {
    name: PropTypes.string
};



