import React from 'react';
import Header from './components/header/header';
import Footer from './components/footer/footer';
import Main from './components/main/main';
import { dataSeed } from './data';

function App() {
  return (
    <React.Fragment>
      <Header title={ 'Header title' }/>
      <Main
        dataObject={dataSeed}
        />
      <Footer
        title={'New title'}
        year={new Date().getFullYear()}
      />
    </React.Fragment>
  );
}

export default App;
